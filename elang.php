<?php

class elang extends hewan{
    use fight;
    function __construct($nama)
    {
        parent::__construct($nama);
        $this->jumlahKaki = 2;
        $this->keahlian = 'terbang tinggi';
        $this->attackPower = 10;
        $this->defencePower = 5; 
    }
    // public $jumlahKaki = 2;
    // public $keahlian = 'terbang tinggi';
    // public $attackPower = 10;
    // public $defencePower = 5;
    public $nama;
    public $jenisHewan = "Elang";
    
    // public function __construct($nama){
    //     $this->nama = $nama;
    // }

    public function __toString()
    {
        echo "{$this->nama}";
    }
    

    public function getInfoHewan(){
        $str = "=======Elang========". "<br>" .
               "Nama : $this->nama". "<br>" .
               "Darah : $this->darah". "<br>" .
               "Jumlah kaki : $this->jumlahKaki". "<br>" .
               "Attack Power : $this->attackPower". "<br>" .
               "Defence Power : $this->defencePower";
        return $str;
      }
    
}
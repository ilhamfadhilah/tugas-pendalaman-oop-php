<?php

class hewan_harimau extends hewan{
    use fight;
    // public $jumlahKaki = 4;
    // public $keahlian = 'lari cepat';
    // public $attackPower = 7;
    // public $defencePower = 8;
    public $nama;
    public $jenisHewan = 'Harimau';


    
    // public function __construct($nama){
    //     $this->nama = $nama;
    // }
    public function __construct($nama){
        // $this->nama = $nama;
        parent::__construct($nama);
        $this->jumlahKaki = 4;
        $this->keahlian = 'lari cepat';
        $this->attackPower = 7;
        $this->defencePower = 8;
    }

    public function set_nama($nama){
        $this->nama = $nama;
    }

    

    public function getInfoHewan(){
        $str = "=======Harimau========". "<br>" .
               "Nama : $this->nama". "<br>" .
               "Darah : $this->darah". "<br>" .
               "Jumlah kaki : $this->jumlahKaki". "<br>" .
               "Attack Power : $this->attackPower". "<br>" .
               "Defence Power : $this->defencePower";
        return $str;       
      }

}

?>